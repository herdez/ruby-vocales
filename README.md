## Ejercicio - Vocales

Define el método `vowels` que recibe como parámetro un string y regresa el número de vocales que tiene ese string.

```ruby
#vowels method



#Driver code

p vowels("hello") == 2
p vowels("Magic") == 2
p vowels("Apologize") == 5
```